import jupyter_client
from shutil import copyfile

copyfile('F:/Internship/SCILAB ML/Machine learning toolbox/Integration Approach/Jupyter_Approach/python_server_lr.py', 'X:/python_server_lr.py')

copyfile('F:/Internship/SCILAB ML/Machine learning toolbox/Integration Approach/Jupyter_Approach/Salary_Data.csv', 'X:/Salary_Data.csv')

cf="X:/kernel-6048.json"
km=jupyter_client.BlockingKernelClient(connection_file=cf)
km.load_connection_file()

km.execute('execfile("python_server_lr.py")')
