pyImport sklearn.svm
svm  = sklearn.svm

pyImport numpy
np = numpy

n_samples = 10
n_features = 5

y = np.random.randn(n_samples)
X = np.random.randn(n_samples, n_features)

classifier = svm.SVR(C = 1, epsilon = 0.2)
classifier.fit(X,y)

support_ = classifier.support_
support_vectors_ = classifier.support_vectors_
dual_coef_ = classifier.dual_coef_
intercept_ = classifier.intercept_
