from sklearn.svm import SVR
import numpy as np
n_samples, n_features = 10, 5
np.random.seed(0)
y = np.random.randn(n_samples)
X = np.random.randn(n_samples, n_features)
clf = SVR(C=1.0, epsilon=0.2)
clf.fit(X, y)

print clf.support_, clf.support_vectors_, clf.dual_coef_, clf.intercept_

support_, support_vectors_, dual_coef_, intercept_
