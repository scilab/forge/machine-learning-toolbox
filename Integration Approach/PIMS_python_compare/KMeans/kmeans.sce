clear
clc

pyImport sklearn.cluster
cluster = sklearn.cluster
pyImport numpy
np = numpy

py = pyBuiltin()

X = np.array([[1, 2]; [1, 4]; [1, 0];[4, 2]; [4, 4]; [4, 0]])
              
kmeans = cluster.KMeans(n_clusters = py.int(2))
kmeans = kmeans.fit(X)

cluster_centers_ = kmeans.cluster_centers_

y_pred = kmeans.predict(np.array([[2,4]; [0,3]]))

x_norms = np.sqrt((X * X).sum(axis=1))

y_pred1 = kmeans._labels_inertia(X, x_norms, cluster_centers_)[0]
