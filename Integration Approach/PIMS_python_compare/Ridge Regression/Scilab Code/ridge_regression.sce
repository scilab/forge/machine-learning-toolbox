clear
clc

// Importing necessary python libraries 
pyImport numpy;
np = numpy;
py = pyBuiltin();


function [result]= predict(X, coef, intercept)
    
    result = X*coef' + intercept
    
endfunction

// Creating data set

X_train = np.array([[0, 0]; [0, 0]; [1, 1]])
y_train = np.array( [0, .1, 1])'
X_test = np.array([[0,1];[1,0];[0,0]])

// Creating and training regression model
pyImport sklearn.linear_model
lm = sklearn.linear_model;
regressor = lm.Ridge(alpha = 0.6);
regressor.fit(X_train, y_train);

coefficients = regressor.coef_
intercepts = regressor.intercept_
alpha = regressor.alpha_ 
//predicting the test set results
y_pred = predict(X_test,coefficients,intercepts);

