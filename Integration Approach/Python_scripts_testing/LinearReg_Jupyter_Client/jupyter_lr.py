from jupyter_client import KernelManager
try:
   from queue import Empty  # Py 3                                                            
except ImportError:
   from Queue import Empty  # Py 2 


def execute (km, cmd):
    c = km.client()
    
    msg_id = c.execute(cmd)
    state = ''
    while state!='idle' and c.is_alive():
        try:
            msg = c.get_iopub_msg(timeout=1)
            print ""
            print msg
            if not 'content' in msg: continue
            content = msg['content']
            print ">>>", content
            if 'execution_state' in content:
                state=content['execution_state']
        except Empty:
            pass


if __name__ == '__main__':
  km = KernelManager()
  km.start_kernel()
  print "Kernel is_alive : ", km.is_alive()

  execute (km, "execfile('simple_linear_regression.py')")
  execute (km, "print regressor.coef_")

  km.shutdown_kernel()
  print "Kernel is_alive : ", km.is_alive()
