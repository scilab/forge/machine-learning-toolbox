from sklearn.kernel_ridge import KernelRidge
import numpy as np
import pickle


n_samples, n_features = 10, 5
rng = np.random.RandomState(0)
y = rng.randn(n_samples)
X = rng.randn(n_samples, n_features)
clf = KernelRidge(alpha=1.0)
clf.fit(X, y)


y_pred = clf.predict(X)

k = KernelRidge._get_kernel(clf,X, clf.X_fit_)

dual_coef = clf.dual_coef_
X_fit_ = clf.X_fit_ 


# Dumping all learned attributes and prediction into a pickle file, to be extarcted later
pickle.dump( [y_pred, dual_coef,X_fit_], open( "attributes.p", "wb" ) )
